<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BalanceDepositTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->unique();
            $table->decimal('balance', 5, 2)->default(0);
            $table->timestamps();
        });
        Schema::create('deposit', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->decimal('deposit', 5, 2)->default(0);
            $table->string('deposit_status')->default('active');
            $table->decimal('percentage', 5, 2)->default(20);
            $table->decimal('accrues_amount', 5, 2)->default(0);
            $table->integer('accrues')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit');
        Schema::dropIfExists('balance');
    }
}
