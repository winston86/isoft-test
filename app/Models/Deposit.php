<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    use HasFactory;

    protected $table = 'deposit';

    protected $fillable = [
		'user_id',
		'deposit',
		'deposit_status',
		'deposit_created_at',
		'percentage',
		'accrues_amount',
		'accrues_qty',
    ];

    public function user()
    {
         return $this->belongsTo(User::class, 'id', 'user_id'); 
    }
}
