<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreDeposit;
use App\Models\Deposit;
use App\Models\Balance;
use Illuminate\Support\Facades\Auth;

class DepositController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function depositForm()
    {
        return view('deposit');
    }

    public function deposit(StoreDeposit $request)
    {
        $data = $request->validated();
        $balance = Balance::where('user_id', Auth::id())->first();

        if ($data['amount'] > 100) {
            return back()->with([
                'msg' => __('Your deposit riched the limit of 100')
            ]);
        }elseif ($data['amount'] < 10) {
            return back()->with([
                'msg' => __('Your minimum deposit should be unless 10')
            ]);
        }elseif ($balance->balance < $data['amount']) {
            return back()->with([
                'msg' => __('Your balance is less than you want to deposit')
            ]);
        }

        $dep = Deposit::create([
            'deposit' => $data['amount'],
            'user_id' => Auth::id()
        ]);

        if (!empty($dep->id)) {
            $balance->balance = $balance->balance - $data['amount'];
            $balance->save();
            $this->log('create_deposit', $data['amount']);
            return redirect(route('home'))->with([
                'msg' => __('You add a deposit of :amount', ['amount' => $data['amount']])
            ]);
        }


    }

}
