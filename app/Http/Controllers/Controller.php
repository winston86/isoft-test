<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Transactions;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected static function log($type, $amount, $user_id = null)
    {
    	$user_id = $user_id ?? Auth::id();
        Transactions::create([
            'type' => $type,
            'user_id' => $user_id,
            'amount' => $amount
        ]);
    }

}
