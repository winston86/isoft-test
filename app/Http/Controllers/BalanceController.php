<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreBalance;
use App\Models\Balance;
use App\Models\Deposit;
use Illuminate\Support\Facades\Auth;

class BalanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function chargeForm()
    {
        return view('charge');
    }

    public function charge(StoreBalance $request)
    {
        $data = $request->validated();
        $res = $this->updateBalance(['amount' => $data['amount']]);

        if ($res) {
            $this->log('enter', $data['amount']);
            return redirect(route('home'))->with([
                'msg' => __('Your balance is charged with :amount', ['amount' => $data['amount']])
            ]);
        }

    }

    public static function accrue()
    {

        $deposites = Deposit::where(
            'deposit_status', 'active',

        )->where('accrues', '<', 10)->get()->all();

        foreach ($deposites as $key => $dep) {
            $amount = $dep->deposit * ($dep->percentage / 100);
            self::updateBalance([
                'amount' => $amount,
                'user_id' => $dep->user_id
            ]);
            $dep->update([
                'accrues' => $dep->accrues++,
                'accrues_amount' => $dep->accrues_amount + $amount
            ]);
            
            parent::log('accrue', $amount, $dep->user_id);

            if ($dep->accrues == 10) {
                $dep->deposit_status = 'closed';
                $dep->save();
                $this->log('close_deposit', $amount);
            }
        }

    }

    private static function updateBalance($data)
    {
        $user_id = $data['user_id'] ?? Auth::id();

        $balance = Balance::where('user_id', $user_id)->first()->balance;

        $data['balance'] = $balance + $data['amount'];

        $data['user_id'] = $user_id;

        unset($data['amount']);

        return Balance::where('user_id', $user_id)->update($data);
    }


}
