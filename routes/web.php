<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/charge-form', [App\Http\Controllers\BalanceController::class, 'chargeForm'])->name('chargeForm');
Route::post('/charge', [App\Http\Controllers\BalanceController::class, 'charge'])->name('charge');

Route::get('/deposit-form', [App\Http\Controllers\DepositController::class, 'depositForm'])->name('depositForm');
Route::post('/deposit', [App\Http\Controllers\DepositController::class, 'deposit'])->name('deposit');
