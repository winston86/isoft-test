@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}<br>
                    @if (!empty(Auth::user()->balance->balance))
                        {{ __('Your balance is') }}: <b>{{ Auth::user()->balance->balance }}</b><br>
                    @endif

                    <div class="p-3">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="deposites-tab" data-toggle="tab" href="#deposites" role="tab" aria-controls="deposites"
                                    aria-selected="true">{{ __('Deposites') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="transactions-tab" data-toggle="tab" href="#transactions" role="tab"
                                    aria-controls="transactions" aria-selected="false">{{ __('Transactions') }}</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="deposites" role="tabpanel" aria-labelledby="deposites-tab">
                                <table class="table">
                                    <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">{{ _('Deposit') }}</th>
                                          <th scope="col">{{ _('Percentage') }}</th>
                                          <th scope="col">{{ _('Accrues') }}</th>
                                          <th scope="col">{{ _('Accrues amount') }}</th>
                                          <th scope="col">{{ _('Status') }}</th>
                                          <th scope="col">{{ _('Date') }}</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach (Auth::user()->deposites as $dep)
                                            <tr>
                                                <td scope="col">{{ $dep->id }}</td>
                                                <td scope="col">{{ $dep->deposit }}</td>
                                                <td scope="col">{{ $dep->percentage }}</td>
                                                <td scope="col">{{ $dep->accrues }}</td>
                                                <td scope="col">{{ $dep->accrues_amount }}</td>
                                                <td scope="col">{{ $dep->deposit_status }}</td>
                                                <td scope="col">{{ $dep->created_at }}</td>
                                            </tr>
                                        @endforeach
                                      </tbody>
                                </table>    
                            </div>
                            <div class="tab-pane fade" id="transactions" role="tabpanel" aria-labelledby="transactions-tab">
                                <table class="table">
                                    <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">{{ _('Type') }}</th>
                                          <th scope="col">{{ _('Amount') }}</th>
                                          <th scope="col">{{ _('Date') }}</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach (Auth::user()->transactions as $trans)
                                            <tr>
                                                <td scope="col">{{ $trans->id }}</td>
                                                <td scope="col">{{ $trans->type }}</td>
                                                <td scope="col">{{ $trans->amount }}</td>
                                                <td scope="col">{{ $trans->created_at }}</td>
                                            </tr>
                                        @endforeach
                                      </tbody>
                                </table> 
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('chargeForm') }}" class="btn btn-primary">{{ __('Charge The Balance') }}</a>
                    <a href="{{ route('depositForm') }}" class="btn btn-primary">{{ __('Deposit From Balance') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
